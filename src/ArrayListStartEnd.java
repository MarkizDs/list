import java.util.Iterator;

public class ArrayListStartEnd implements EList, Iterable<Integer> {

    int[] ar = new int[30];
    int start = 0;
    int end = 0;

    @Override
    public String toString() {
        String str = "";

        for (int i = start; i < end; i++) {
            str += ar[i];
            if (i < end - 1)
                str += ",";
        }
        return str;
    }

    @Override
    public void clear() {
        start = 0;
        end = 0;

    }

    @Override
    public void init(int[] ini) {
        if (ini == null)
            ini = new int[0];

        int dx = (ini.length % 2 == 0) ? 0 : 1;
        start = ar.length / 2 - ini.length / 2 - dx;
        end = ar.length / 2 + ini.length / 2;

        for (int i = 0; i < ini.length; i++) {
            ar[start + i] = ini[i];
        }
    }

    @Override
    public int[] toArray() {
        int[] tmp = new int[end - start];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = ar[start + i];
        }
        return tmp;
    }

    @Override
    public int size() {
        int tmp = end - start;
        return tmp;
    }

    @Override
    public void print() {
        for (int i = start; i < end; i++) {
            System.out.print(ar[i]);
        }
        System.out.println();
    }

    @Override
    public void addStart(int val) {
        ar[start - 1] = val;
        start = start - 1;
    }

    @Override
    public void addEnd(int val) {
        ar[end] = val;
        end = end + 1;
    }

    @Override
    public void addPos(int pos, int val) {

        if (start + end == 0)
            throw new IllegalArgumentException();

        for (int i = end; i > start + pos; i--) {
            ar[i] = ar[i - 1];
        }
        ar[start + pos] = val;
        end++;
    }

    @Override
    public int delStart() {
        if (end - start == 0)
            throw new IllegalArgumentException();
        int val = ar[start];

        start = start + 1;
        return val;
    }

    @Override
    public int delEnd() {
        if (end - start == 0)
            throw new IllegalArgumentException();
        int val = ar[end - 1];
        end--;
        return val;
    }

    @Override
    public int delPos(int pos) {
        if (end - start == 0)
            throw new IllegalArgumentException();
        int ret = ar[start + pos];
        for (int i = start + pos; i < end; i++) {
            ar[i] = ar[i + 1];
        }
        end--;
        return ret;
    }

    @Override
    public int min() {
        int val = Integer.MAX_VALUE;
        for (int i = start; i < end; i++) {
            if (val > ar[i]) {
                val = ar[i];
            }
        }
        return val;
    }

    @Override
    public int max() {

        int val = Integer.MIN_VALUE;
        for (int i = start; i < end; i++) {
            if (val < ar[i]) {
                val = ar[i];
            }
        }
        return val;
    }

    @Override
    public int minPos() {
        int res = start;
        for (int i = start; i < end; i++) {
            if (ar[res] > ar[i]) {
                res = i;
            }
        }
        return res - start;
    }

    @Override
    public int maxPos() {
        int res = start;
        for (int i = start; i < end; i++) {
            if (ar[res] < ar[i]) {
                res = i;
            }
        }
        return res - start;
    }

    @Override
    public void sort() {
        for (int i = start; i < end - 1; i++) {
            for (int j = start; j < end - 1; j++) {
                if (ar[j] > ar[j + 1]) {
                    int tmp = ar[j];
                    ar[j] = ar[j + 1];
                    ar[j + 1] = tmp;
                }
            }
        }

    }

    @Override
    public void reverse() {
        int s = size();
        for (int i = start; i < end - s / 2; i++) {
            int temp = ar[i];
            ar[i] = ar[end - (i + 1 - start)];
            ar[end - (i + 1 - start)] = temp;
        }

    }

    @Override
    public void halfReverse() {
        int s = size();
        int dx = (s % 2 == 0) ? 0 : 1;

        for (int i = start; i < start + s / 2; i++) {
            int temp = ar[i];
            ar[i] = ar[(start + s / 2) + (i - start) + dx];
            ar[(start + s / 2) + (i - start) + dx] = temp;
        }

    }

    @Override
    public int get(int pos) {
        int val = ar[start + pos];
        return val;
    }

    @Override
    public void set(int pos, int val) {
        ar[start + pos] = val;

    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyIter(ar);
    }

    class MyIter implements Iterator<Integer> {
        int[] ar = null;
        int i = start;

        public MyIter(int[] ar) {
            this.ar = ar;
        }

        @Override
        public boolean hasNext() {
            return i < end;
        }

        @Override
        public Integer next() {
            int ret = ar[i];
            i++;
            return ret;
        }

    }

}
