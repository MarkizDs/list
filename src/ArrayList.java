import java.util.Iterator;

public class ArrayList implements EList, Iterable<Integer> {
    int[] ar = new int[0];

    @Override
    public String toString() {
        String str = "";

        for (int i = 0; i < ar.length; i++) {
            str += ar[i];
            if (i < ar.length - 1)
                str += ",";
        }
        return str;
    }

    public void clear() {
        ar = new int[0];
    }

    public int size() {
        return ar.length;
    }

    public void init(int[] ini) {
        if (ini == null)
            ini = new int[0];

        ar = new int[ini.length];
        for (int i = 0; i < ini.length; i++) {
            ar[i] = ini[i];
        }
    }

    public int[] toArray() {
        int[] tmp = new int[ar.length];
        for (int i = 0; i < ar.length; i++) {
            tmp[i] = ar[i];
        }
        return tmp;
    }

    public void addStart(int val) {
        int[] tmp = new int[ar.length + 1];
        for (int i = 0; i < ar.length; i++) {
            tmp[i + 1] = ar[i];
        }
        tmp[0] = val;
        ar = tmp;
    }

    public void addEnd(int val) {
        int[] tmp = new int[ar.length + 1];
        for (int i = 0; i < ar.length; i++) {
            tmp[i] = ar[i];
        }
        tmp[ar.length] = val;
        ar = tmp;
    }

    public void sort() {
        for (int i = 0; i < ar.length - 1; i++) {
            for (int j = 0; j < ar.length - 1; j++) {
                if (ar[j] > ar[j + 1]) {
                    int tmp = ar[j];
                    ar[j] = ar[j + 1];
                    ar[j + 1] = tmp;
                }
            }
        }
    }

    public void print() {
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i]);
        }
        System.out.println();
    }

    public void addPos(int pos, int val) {

        int[] mass = new int[ar.length + 1];
        for (int i = 0; i < pos; i++) {
            mass[i] = ar[i];
        }
        mass[pos] = val;
        for (int i = pos + 1; i < mass.length; i++) {
            mass[i] = ar[i - 1];
        }
        ar = mass;
    }

    public int delStart() {
        int val = 0;
        if (ar.length == 0)
            throw new IllegalArgumentException();

        int[] mass = new int[ar.length - 1];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = ar[i + 1];
        }
        val = ar[0];
        ar = mass;
        return val;

    }

    public int delEnd() {
        if (ar.length == 0)
            throw new IllegalArgumentException();
        int val = ar[ar.length - 1];
        int[] mass = new int[ar.length - 1];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = ar[i];
        }
        ar = mass;
        return val;
    }

    public int delPos(int pos) {
        if (ar.length == 0)
            throw new IllegalArgumentException();
        int val = ar[pos];
        int[] mass = new int[ar.length - 1];
        for (int i = 0; i < pos; i++) {
            mass[i] = ar[i];
        }
        for (int i = pos; i < mass.length; i++) {
            mass[i] = ar[i + 1];
        }
        ar = mass;
        return val;
    }

    public int min() {

        int val = Integer.MAX_VALUE;
        for (int i = 0; i < ar.length; i++) {
            if (val > ar[i]) {
                val = ar[i];
            }
        }
        return val;
    }

    public int max() {

        int val = Integer.MIN_VALUE;
        for (int i = 0; i < ar.length; i++) {
            if (val < ar[i]) {
                val = ar[i];
            }
        }
        return val;
    }

    public int minPos() {

        int ret = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[ret] > ar[i]) {
                ret = i;
            }
        }
        return ret;
    }

    public int maxPos() {

        int ret = 0;
        for (int i = 0; i < ar.length; i++) {
            if (ar[ret] < ar[i]) {
                ret = i;
            }
        }
        return ret;
    }

    public void reverse() {

        for (int i = 0; i < ar.length / 2; i++) {
            int temp = ar[i];
            ar[i] = ar[ar.length - (i + 1)];
            ar[ar.length - (i + 1)] = temp;
        }
    }

    public void halfReverse() {
        int dx = (ar.length % 2 == 0) ? 0 : 1;
        for (int i = 0; i < ar.length / 2; i++) {
            int tmp = ar[i];
            ar[i] = ar[ar.length / 2 + i + dx];
            ar[ar.length / 2 + i + dx] = tmp;
        }

    }

    public int get(int pos) {

        int val = ar[pos];
        return val;
    }

    public void set(int pos, int val) {

        ar[pos] = val;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyIter(ar);
    }

    class MyIter implements Iterator<Integer> {
        int[] ar = null;
        int i = 0;

        public MyIter(int[] ar) {
            this.ar = ar;
        }

        @Override
        public boolean hasNext() {
            return i < ar.length;
        }

        @Override
        public Integer next() {
            int ret = ar[i];
            i++;
            return ret;
        }
    }

}
