

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArrayListTest {
    //================================
    // size
    //================================

    @Test
    public void testSizeNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        assertEquals(0, rr.size());
    }

    @Test
    public void testSizeZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        assertEquals(0, rr.size());
    }

    @Test
    public void testSizeOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        assertEquals(1, rr.size());
    }

    @Test
    public void testSizeTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        assertEquals(2, rr.size());
    }

    @Test
    public void testSizeMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        assertEquals(6, rr.size());
    }

    //================================
    // toArray
    //================================
    @Test
    public void testToArrayNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {10, 20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // clear
    //================================
    @Test
    public void testClearNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.clear();
        assertEquals(0, rr.size());
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.clear();
        assertEquals(0, rr.size());
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.clear();
        assertEquals(0, rr.size());
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.clear();
        assertEquals(0, rr.size());
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.clear();
        assertEquals(0, rr.size());
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    //================================
    // addStart
    //================================
    @Test
    public void testAddStartZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addStart(99);
        assertEquals(1, rr.size());
        int[] act = rr.toArray();
        int[] exp = {99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddStartOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addStart(99);
        assertEquals(2, rr.size());
        int[] act = rr.toArray();
        int[] exp = {99, 10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddStartTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addStart(99);
        assertEquals(3, rr.size());
        int[] act = rr.toArray();
        int[] exp = {99, 10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddStartMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addStart(99);
        assertEquals(7, rr.size());
        int[] act = rr.toArray();
        int[] exp = {99, 10, 20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // addEnd
    //================================
    @Test
    public void testAddEndZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addEnd(99);
        assertEquals(1, rr.size());
        int[] act = rr.toArray();
        int[] exp = {99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEndOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addEnd(99);
        assertEquals(2, rr.size());
        int[] act = rr.toArray();
        int[] exp = {10, 99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEndTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addEnd(99);
        assertEquals(3, rr.size());
        int[] act = rr.toArray();
        int[] exp = {10, 20, 99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEndMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addEnd(99);
        assertEquals(7, rr.size());
        int[] act = rr.toArray();
        int[] exp = {10, 20, 77, 11, 24, 82, 99};
        assertArrayEquals(exp, act);
    }

    //================================
    // sort
    //================================
    @Test
    public void testSortZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.sort();
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSortOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.sort();
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSortTwo() {
        int[] ini = {30, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.sort();
        int[] act = rr.toArray();
        int[] exp = {20, 30};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSortMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.sort();
        int[] act = rr.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // toString
    //================================
    @Test
    public void testToStringZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        String act = rr.toString();
        String exp = "";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        String act = rr.toString();
        String exp = "10";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        String act = rr.toString();
        String exp = "10,20";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        String act = rr.toString();
        String exp = "10,20,77,11,24,82";
        assertEquals(exp, act);
    }

    //================================
    // init
    //================================
    @Test
    public void testInitNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitZero() {
        int[] ini = {};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int[] act = rr.toArray();
        int[] exp = {10, 20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }
    //================================
    // addPos
    //================================


    @Test
    public void testAddPosOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addPos(1, 2);
        int[] act = rr.toArray();
        int[] exp = {10, 2};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddPosTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addPos(1, 1);
        int[] act = rr.toArray();
        int[] exp = {10, 1, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddPosMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.addPos(3, 0);
        int[] act = rr.toArray();
        int[] exp = {10, 20, 77, 0, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // delStart
    //================================
    @Test(expected = IllegalArgumentException.class)
    public void testDelStartNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delStart();

    }

    @Test
    public void testDelStartOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delStart();
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelStartTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delStart();
        int[] act = rr.toArray();
        int[] exp = {20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelStartMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delStart();
        int[] act = rr.toArray();
        int[] exp = {20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // delEnd
    //================================
    @Test(expected = IllegalArgumentException.class)
    public void testDelEndNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delEnd();

    }

    @Test
    public void testDelEndOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delEnd();
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelEndTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delEnd();
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelEndMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delEnd();
        int[] act = rr.toArray();
        int[] exp = {10, 20, 77, 11, 24};
        assertArrayEquals(exp, act);
    }

    //================================
    // delPos
    //================================
    @Test(expected = IllegalArgumentException.class)
    public void testDelPosNull() {
        int[] ini = null;
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delPos(1);

    }

    @Test
    public void testDelPosOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delPos(0);
        int[] act = rr.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelPosTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delPos(1);
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelPosMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.delPos(3);
        int[] act = rr.toArray();
        int[] exp = {10, 20, 77, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // Min
    //================================


    @Test
    public void testMinOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.min();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testMinTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.min();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testMinMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.min();
        int exp = 10;
        assertEquals(exp, act);
    }
    //================================
    // Max
    //================================


    @Test
    public void testMaxOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.max();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.max();
        int exp = 20;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.max();
        int exp = 82;
        assertEquals(exp, act);
    }
    //================================
    // MinPos
    //================================


    @Test
    public void testMinPosOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.minPos();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void testMinPosTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.minPos();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void testMinPosMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.minPos();
        int exp = 3;
        assertEquals(exp, act);
    }
    //================================
    // MaxPos
    //================================


    @Test
    public void testMaxPosOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.maxPos();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxPosTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.maxPos();
        int exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxPosMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.maxPos();
        int exp = 5;
        assertEquals(exp, act);
    }
    //================================
    // Reverse
    //================================


    @Test
    public void testReverseOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.reverse();
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.reverse();
        int[] act = rr.toArray();
        int[] exp = {20, 10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.reverse();
        int[] act = rr.toArray();
        int[] exp = {82, 24, 11, 77, 20, 10};
        assertArrayEquals(exp, act);
    }
    //================================
    // HalfReverse
    //================================


    @Test
    public void testHalfReverseOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.halfReverse();
        int[] act = rr.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testHalfReverseTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.halfReverse();
        int[] act = rr.toArray();
        int[] exp = {20, 10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testHalfReverseMany() {
        int[] ini = {10, 20, 77, 2, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.halfReverse();
        int[] act = rr.toArray();
        int[] exp = {11, 24, 82, 2, 10, 20, 77};
        assertArrayEquals(exp, act);
    }
    //================================
    // Get
    //================================


    @Test
    public void testGetOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.get(0);
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testGetTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.get(1);
        int exp = 20;
        assertEquals(exp, act);
    }

    @Test
    public void testGetMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.get(3);
        int exp = 11;
        assertEquals(exp, act);
    }
    //================================
    // Set
    //================================


    @Test
    public void testSetOne() {
        int[] ini = {10};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        rr.set(0, 2);
        int[] act = rr.toArray();
        int[] exp = {2};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSetTwo() {
        int[] ini = {10, 20};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.get(1);
        int exp = 20;
        assertEquals(exp, act);
    }

    @Test
    public void testSetMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};
        ArrayList rr = new ArrayList();
        rr.init(ini);
        int act = rr.get(3);
        int exp = 11;
        assertEquals(exp, act);
    }


}
