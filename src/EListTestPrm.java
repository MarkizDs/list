import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class EListTestPrm {
    EList obj = null;

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]
                {
                        //{ new ArrayList() },
                        //{ new ArrayListIndex() },
                        //{ new ArrayListStartEnd() },
                        {new LinkedList()},
                        {new LinkeListStartEnd()}
                });
    }

    public EListTestPrm(EList prm) {
        obj = prm;
    }

    @Before
    public void setUp() {
        obj.clear();
    }

    @Test
    public void testSizeNull() {
        int[] ini = null;
        obj.init(ini);
        assertEquals(0, obj.size());
    }

    @Test
    public void testSizeZero() {
        int[] ini = {};

        obj.init(ini);
        assertEquals(0, obj.size());
    }

    @Test
    public void testSizeOne() {
        int[] ini = {10};

        obj.init(ini);
        assertEquals(1, obj.size());
    }

    @Test
    public void testSizeTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        assertEquals(2, obj.size());
    }

    @Test
    public void testSizeMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        assertEquals(6, obj.size());
    }

    //================================
    // toArray
    //================================
    @Test
    public void testToArrayNull() {
        int[] ini = null;

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayZero() {
        int[] ini = {};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayOne() {
        int[] ini = {10};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // clear
    //================================
    @Test
    public void testClearNull() {
        int[] ini = null;

        obj.init(ini);
        obj.clear();
        assertEquals(0, obj.size());
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearZero() {
        int[] ini = {};

        obj.init(ini);
        obj.clear();
        assertEquals(0, obj.size());
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.clear();
        assertEquals(0, obj.size());
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.clear();
        assertEquals(0, obj.size());
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.clear();
        assertEquals(0, obj.size());
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    //================================
    // addStart
    //================================
    @Test
    public void testAddStartZero() {
        int[] ini = {};

        obj.init(ini);
        obj.addStart(99);
        assertEquals(1, obj.size());
        int[] act = obj.toArray();
        int[] exp = {99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddStartOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.addStart(99);
        assertEquals(2, obj.size());
        int[] act = obj.toArray();
        int[] exp = {99, 10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddStartTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.addStart(99);
        assertEquals(3, obj.size());
        int[] act = obj.toArray();
        int[] exp = {99, 10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddStartMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.addStart(99);
        assertEquals(7, obj.size());
        int[] act = obj.toArray();
        int[] exp = {99, 10, 20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // addEnd
    //================================
    @Test
    public void testAddEndZero() {
        int[] ini = {};

        obj.init(ini);
        obj.addEnd(99);
        assertEquals(1, obj.size());
        int[] act = obj.toArray();
        int[] exp = {99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEndOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.addEnd(99);
        assertEquals(2, obj.size());
        int[] act = obj.toArray();
        int[] exp = {10, 99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEndTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.addEnd(99);
        assertEquals(3, obj.size());
        int[] act = obj.toArray();
        int[] exp = {10, 20, 99};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEndMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.addEnd(99);
        assertEquals(7, obj.size());
        int[] act = obj.toArray();
        int[] exp = {10, 20, 77, 11, 24, 82, 99};
        assertArrayEquals(exp, act);
    }

    //================================
    // sort
    //================================
    @Test
    public void testSortZero() {
        int[] ini = {};

        obj.init(ini);
        obj.sort();
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSortOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.sort();
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSortTwo() {
        int[] ini = {30, 20};

        obj.init(ini);
        obj.sort();
        int[] act = obj.toArray();
        int[] exp = {20, 30};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSortMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.sort();
        int[] act = obj.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // toString
    //================================
    @Test
    public void testToStringZero() {
        int[] ini = {};

        obj.init(ini);
        String act = obj.toString();
        String exp = "";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringOne() {
        int[] ini = {10};

        obj.init(ini);
        String act = obj.toString();
        String exp = "10";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        String act = obj.toString();
        String exp = "10,20";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        String act = obj.toString();
        String exp = "10,20,77,11,24,82";
        assertEquals(exp, act);
    }//================================

    // init
    //================================
    @Test
    public void testInitNull() {
        int[] ini = null;

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitZero() {
        int[] ini = {};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitOne() {
        int[] ini = {10};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }
    //================================
    // addPos
    //================================

    @Test
    public void testAddPosOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.addPos(1, 2);
        int[] act = obj.toArray();
        int[] exp = {10, 2};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddPosTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.addPos(1, 1);
        int[] act = obj.toArray();
        int[] exp = {10, 1, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddPosMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.addPos(3, 0);
        int[] act = obj.toArray();
        int[] exp = {10, 20, 77, 0, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // delStart
    //================================
    @Test(expected = IllegalArgumentException.class)
    public void testDelStartNull() {
        int[] ini = null;

        obj.init(ini);
        obj.delStart();

    }

    @Test
    public void testDelStartOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.delStart();
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelStartTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.delStart();
        int[] act = obj.toArray();
        int[] exp = {20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelStartMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.delStart();
        int[] act = obj.toArray();
        int[] exp = {20, 77, 11, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // delEnd
    //================================
    @Test(expected = IllegalArgumentException.class)
    public void testDelEndNull() {
        int[] ini = null;

        obj.init(ini);
        obj.delEnd();

    }

    @Test
    public void testDelEndOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.delEnd();
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelEndTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.delEnd();
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelEndMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.delEnd();
        int[] act = obj.toArray();
        int[] exp = {10, 20, 77, 11, 24};
        assertArrayEquals(exp, act);
    }

    //================================
    // delPos
    //================================
    @Test(expected = IllegalArgumentException.class)
    public void testDelPosNull() {
        int[] ini = null;

        obj.init(ini);
        obj.delPos(1);

    }

    @Test
    public void testDelPosOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.delPos(0);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelPosTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.delPos(1);
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelPosMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.delPos(3);
        int[] act = obj.toArray();
        int[] exp = {10, 20, 77, 24, 82};
        assertArrayEquals(exp, act);
    }

    //================================
    // Min
    //================================


    @Test
    public void testMinOne() {
        int[] ini = {10};

        obj.init(ini);
        int act = obj.min();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testMinTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int act = obj.min();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testMinMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        int act = obj.min();
        int exp = 10;
        assertEquals(exp, act);
    }
    //================================
    // Max
    //================================


    @Test
    public void testMaxOne() {
        int[] ini = {10};

        obj.init(ini);
        int act = obj.max();
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int act = obj.max();
        int exp = 20;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        int act = obj.max();
        int exp = 82;
        assertEquals(exp, act);
    }
    //================================
    // MinPos
    //================================


    @Test
    public void testMinPosOne() {
        int[] ini = {10};

        obj.init(ini);
        int act = obj.minPos();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void testMinPosTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int act = obj.minPos();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void testMinPosMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};

        obj.init(ini);
        int act = obj.minPos();
        int exp = 3;
        assertEquals(exp, act);
    }
    //================================
    // MaxPos
    //================================

    @Test
    public void testMaxPosOne() {
        int[] ini = {10};

        obj.init(ini);
        int act = obj.maxPos();
        int exp = 0;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxPosTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int act = obj.maxPos();
        int exp = 1;
        assertEquals(exp, act);
    }

    @Test
    public void testMaxPosMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};

        obj.init(ini);
        int act = obj.maxPos();
        int exp = 5;
        assertEquals(exp, act);
    }
    //================================
    // Reverse
    //================================


    @Test
    public void testReverseOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {20, 10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};

        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {82, 24, 11, 77, 20, 10};
        assertArrayEquals(exp, act);
    }
    //================================
    // HalfReverse
    //================================

    @Test
    public void testHalfReverseOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.halfReverse();
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testHalfReverseTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        obj.halfReverse();
        int[] act = obj.toArray();
        int[] exp = {20, 10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testHalfReverseMany() {
        int[] ini = {10, 20, 77, 2, 11, 24, 82};

        obj.init(ini);
        obj.halfReverse();
        int[] act = obj.toArray();
        int[] exp = {2, 11, 24, 82, 10, 20, 77};
        assertArrayEquals(exp, act);
    }
    //================================
    // Get
    //================================

    @Test
    public void testGetOne() {
        int[] ini = {10};

        obj.init(ini);
        int act = obj.get(0);
        int exp = 10;
        assertEquals(exp, act);
    }

    @Test
    public void testGetTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int act = obj.get(1);
        int exp = 20;
        assertEquals(exp, act);
    }

    @Test
    public void testGetMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};

        obj.init(ini);
        int act = obj.get(3);
        int exp = 11;
        assertEquals(exp, act);
    }
    //================================
    // Set
    //================================

    @Test
    public void testSetOne() {
        int[] ini = {10};

        obj.init(ini);
        obj.set(0, 2);
        int[] act = obj.toArray();
        int[] exp = {2};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSetTwo() {
        int[] ini = {10, 20};

        obj.init(ini);
        int act = obj.get(1);
        int exp = 20;
        assertEquals(exp, act);
    }

    @Test
    public void testSetMany() {
        int[] ini = {12, 20, 77, 11, 24, 82};

        obj.init(ini);
        int act = obj.get(3);
        int exp = 11;
        assertEquals(exp, act);
    }
}
