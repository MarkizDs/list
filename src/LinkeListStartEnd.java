import java.util.Iterator;

public class LinkeListStartEnd implements EList, Iterable<Integer> {
    class Node {

        int val;
        Node next = null;
        Node prev = null;

        public Node(int val) {
            this.val = val;
        }
    }

    Node root = null;
    Node last = null;

    @Override
    public void clear() {
        root = null;
        last = null;
    }

    @Override
    public void init(int[] ini) {
        if (ini == null)
            ini = new int[0];
        for (int i = ini.length - 1; i >= 0; i--) {
            addStart(ini[i]);
        }

    }


    @Override
    public int[] toArray() {
        int[] ret = new int[size()];
        Node p = root;
        for (int i = 0; i < ret.length; i++) {
            ret[i] = p.val;
            p = p.next;
        }
        return ret;
    }

    @Override
    public int size() {
        int count = 0;

        Node p = root;
        while (p != null) {
            count++;
            p = p.next;
        }
        return count;
    }

    @Override
    public void addStart(int val) {
        Node temp = new Node(val);
        if (root == null) {
            root = temp;
            last = temp;
        } else {
            temp.next = root;
            root = temp;
            root.prev = temp;
            temp.prev = null;
        }
    }

    @Override
    public void addEnd(int val) {
        Node toLast = new Node(val);

        if (root == null) {
            root = toLast;
            last = toLast;
            return;
        }

        if (size() == 1) {
            last = toLast;
            last.prev = root;
            root.next = last;
            return;
        }
        last.next = toLast;
        toLast.prev = last;
        last = toLast;
    }

    @Override
    public void addPos(int pos, int val) {
        Node tmp = root;
        if (pos == 0)
            addStart(val);
        else {
            for (int i = 1; i < pos; i++) {
                tmp = tmp.next;
            }
            Node obj = new Node(val);
            obj.next = tmp.next;
            tmp.next = obj;
            Node tmplast = getNode(size() - 1);
            last = tmplast;
        }
    }

    @Override
    public int delStart() {
        if (root == null)
            throw new IllegalArgumentException();
        Node p = root;
        root = p.next;
        int tmp = p.val;
        Node tmplast = getNode(size() - 1);
        last = tmplast;
        return tmp;
    }

    @Override
    public int delEnd() {
        if (root == null)
            throw new IllegalArgumentException();
        int ret = 0;
        Node temp = root;
        ret = temp.val;

        if (size() == 1) {
            clear();
            return ret;
        }
        for (int i = 1; i < size() - 1; i++) {
            temp = temp.next;
        }
        ret = temp.next.val;
        temp.next = null;
        Node tmplast = getNode(size() - 1);
        last = tmplast;
        return ret;
    }

    @Override
    public int delPos(int pos) {
        if (root == null)
            throw new IllegalArgumentException();
        Node tmp = root;
        int ret = 0;
        if (pos == 0 || size() == 1) {
            delStart();
            ret = tmp.val;
        } else {
            for (int i = 1; i < pos; i++) {
                tmp = tmp.next;
            }
            ret = tmp.next.val;
            tmp.next = tmp.next.next;
        }
        Node tmplast = getNode(size() - 1);
        last = tmplast;
        return ret;

    }

    @Override
    public int min() {
        Node p = root;
        int ret = Integer.MAX_VALUE;
        while (p != null) {
            if (p.val < ret) {
                ret = p.val;
            }
            p = p.next;
        }
        return ret;
    }

    @Override
    public int max() {
        Node p = root;
        int ret = Integer.MIN_VALUE;
        while (p != null) {
            if (p.val > ret) {
                ret = p.val;
            }
            p = p.next;
        }
        return ret;
    }

    @Override
    public int minPos() {
        Node p = root;
        int k = 0;
        int ret = Integer.MAX_VALUE;
        for (int i = 0; i < size(); i++) {
            if (p.val < ret) {
                ret = p.val;
                k = i;
            }
            p = p.next;
        }
        return k;
    }

    @Override
    public int maxPos() {
        Node p = root;
        int k = 0;
        int ret = Integer.MIN_VALUE;
        for (int i = 0; i < size(); i++) {
            if (p.val > ret) {
                ret = p.val;
                k = i;
            }
            p = p.next;
        }
        return k;
    }

    @Override
    public void sort() {
        Node p = root;
        for (int i = 0; i < size() - 1; i++) {
            while (p.next != null) {
                if (p.val > p.next.val) {
                    int tmp = p.val;
                    p.val = p.next.val;
                    p.next.val = tmp;
                }
                p = p.next;
            }
            p = root;
        }
        Node tmplast = getNode(size() - 1);
        last = tmplast;
    }

    @Override
    public void reverse() {
        Node reversedPart = null;
        Node current = root;
        while (current != null) {
            Node next = current.next;
            current.next = reversedPart;
            reversedPart = current;
            current = next;
        }
        root = reversedPart;
        Node tmplast = getNode(size() - 1);
        last = tmplast;
    }

    @Override
    public void halfReverse() {
        if (size() == 1)
            return;
        Node middle = getNode(size() / 2);
        Node tempRoot = root;
        Node preMiddle = getNode((size() / 2) - 1);
        preMiddle.next = null;
        root = middle;
        Node last = getNode(size() - 1);
        last.next = tempRoot;
        Node tmplast = getNode(size() - 1);
        last = tmplast;
    }

    @Override
    public int get(int pos) {
        Node p = root;
        int ret = 0;
        for (int i = 0; i <= pos; i++) {
            ret = p.val;
            p = p.next;

        }
        return ret;
    }


    @Override
    public void set(int pos, int val) {
        Node p = root;
        for (int i = 0; i <= pos; i++) {
            if (i == pos) {
                p.val = val;
            }
            p = p.next;

        }

    }

    @Override
    public void print() {
        Node p = root;

        while (p != null) {
            System.out.print(p.val + ",");
            p = p.next;
        }
        System.out.println();
    }

    public Node getNode(int pos) {
        Node p = root;
        Node ret = null;
        for (int i = 0; i <= pos; i++) {
            ret = p;
            p = p.next;

        }
        return ret;
    }

    public void setNode(int pos, Node cur) {
        Node p = root;
        for (int i = 0; i <= pos; i++) {
            if (i == pos) {
                p = cur;
            }
            p = p.next;

        }

    }

    @Override
    public String toString() {
        Node p = root;
        String string = "";

        while (p != null) {
            if (p != root) {
                string += ",";
            }
            string += p.val;
            p = p.next;
        }
        return string;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyIter(root);
    }

    class MyIter implements Iterator<Integer> {
        Node p = null;

        public MyIter(Node p) {
            this.p = p;
        }

        @Override
        public boolean hasNext() {
            return p != null;
        }

        @Override
        public Integer next() {
            int ret = p.val;
            p = p.next;
            return ret;
        }
    }
}
