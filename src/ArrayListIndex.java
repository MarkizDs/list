import java.util.Iterator;

public class ArrayListIndex implements EList, Iterable<Integer> {
    int[] ar = new int[10];
    int index = 0;

    @Override
    public String toString() {
        String str = "";

        for (int i = 0; i < index; i++) {
            str += ar[i];
            if (i < index - 1)
                str += ",";
        }
        return str;
    }

    public void clear() {
        index = 0;
    }

    public int size() {
        return index;
    }

    public void init(int[] ini) {
        if (ini == null)
            ini = new int[0];


        for (int i = 0; i < ini.length; i++) {
            ar[i] = ini[i];
        }
        index = ini.length;
    }

    public int[] toArray() {
        int[] tmp = new int[index];
        for (int i = 0; i < index; i++) {
            tmp[i] = ar[i];
        }
        return tmp;
    }

    public void addStart(int val) {

        for (int i = index; i > 0; i--) {
            ar[i] = ar[i - 1];
        }
        ar[0] = val;
        index++;
    }

    public void addEnd(int val) {

        ar[index++] = val;
    }

    public void sort() {
        for (int i = 0; i < index - 1; i++) {
            for (int j = 0; j < index - 1; j++) {
                if (ar[j] > ar[j + 1]) {
                    int tmp = ar[j];
                    ar[j] = ar[j + 1];
                    ar[j + 1] = tmp;
                }
            }
        }
    }

    public void print() {
        for (int i = 0; i < index; i++) {
            System.out.print(ar[i]);
        }
        System.out.println();
    }

    public void addPos(int pos, int val) {

        for (int i = index; i > pos; i--) {
            ar[i] = ar[i - 1];
        }
        ar[pos] = val;
        index++;

    }

    public int delStart() {
        if (index == 0)
            throw new IllegalArgumentException();
        int val = ar[0];
        for (int i = 0; i < index; i++) {
            ar[i] = ar[i + 1];
        }
        index--;
        return val;
    }

    public int delEnd() {
        if (index == 0)
            throw new IllegalArgumentException();
        int val = ar[index - 1];
        index--;
        return val;
    }

    public int delPos(int pos) {
        if (index == 0)
            throw new IllegalArgumentException();
        int ret = ar[pos];
        for (int i = pos; i < index; i++) {
            ar[i] = ar[i + 1];
        }
        index--;
        return ret;
    }

    public int min() {

        int val = Integer.MAX_VALUE;
        for (int i = 0; i < index; i++) {
            if (val > ar[i]) {
                val = ar[i];
            }
        }
        return val;
    }

    public int max() {

        int val = Integer.MIN_VALUE;
        for (int i = 0; i < index; i++) {
            if (val < ar[i]) {
                val = ar[i];
            }
        }
        return val;
    }

    public int minPos() {

        int ret = 0;
        for (int i = 0; i < index; i++) {
            if (ar[ret] > ar[i]) {
                ret = i;
            }
        }
        return ret;
    }

    public int maxPos() {


        int ret = 0;
        for (int i = 0; i < index; i++) {
            if (ar[ret] < ar[i]) {
                ret = i;
            }
        }
        return ret;
    }

    public void reverse() {

        for (int i = 0; i < index / 2; i++) {
            int temp = ar[i];
            ar[i] = ar[index - (i + 1)];
            ar[index - (i + 1)] = temp;
        }
    }

    public void halfReverse() {

        int dx = (index % 2 == 0) ? 0 : 1;
        for (int i = 0; index / 2 > i; i++) {
            int temp = ar[i];
            ar[i] = ar[index / 2 + i + dx];
            ar[index / 2 + i + dx] = temp;
        }

    }

    public int get(int pos) {

        int val = ar[pos];
        return val;
    }

    public void set(int pos, int val) {

        ar[pos] = val;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyIter(ar);
    }

    class MyIter implements Iterator<Integer> {
        int[] ar = null;
        int i = 0;

        public MyIter(int[] ar) {
            this.ar = ar;
        }

        @Override
        public boolean hasNext() {
            return i < index;
        }

        @Override
        public Integer next() {
            int ret = ar[i];
            i++;
            return ret;
        }

    }
}


